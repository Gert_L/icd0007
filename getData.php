<?php
/**
 * Created by PhpStorm.
 * User: gertlohmus
 * Date: 2019-03-21
 * Time: 19:40
 */

const DATA_FILE = "data.txt";

function get_data_entries() {
    return file(DATA_FILE, FILE_IGNORE_NEW_LINES);
}

function add_data_entry($contacts) {
    if (isset($contacts)){
        file_put_contents(DATA_FILE, join(",", $contacts) . PHP_EOL, FILE_APPEND);
        }
}