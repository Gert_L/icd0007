<?php
$connection = new PDO('sqlite:db1.sqlite'); $connection->setAttribute(PDO::ATTR_ERRMODE,
    PDO::ERRMODE_EXCEPTION);

$stmt = $connection->prepare(
    'CREATE TABLE contacts (
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    firstName VARCHAR(255),
    lastName VARCHAR(255))');


$stmt->execute();

$stmt = $connection->prepare(
    'CREATE TABLE phones (
    contact_id INTEGER, 
    number VARCHAR(255))');


$stmt->execute();
