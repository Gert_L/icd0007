<?php
require_once("contactsDAO.php");
require_once("sqliteContactApp.php");
require_once("Contact.php");
require_once("lib/tpl.php");

$page = isset($_GET['command'])? $_GET["command"] : "contactList";
session_start();
$contactsDAO = new SqliteContactsDao();
function validate_contact($contact) {
    $errorMessages = [];

    if (empty($contact)) {
        $errorMessages[] = "Todo item ei või olla tühi!";
    }

    if (strlen($contact[0]) < 2 or strlen($contact[1]) < 2) {
        $errorMessages[] = "Pikkus peab olema vähemalt 3 tähemärki";
    }

    return $errorMessages;
}



if ($page == "contactList") {
    $contacts = $contactsDAO->findAll();
    $data = ['$contacts' => $contacts];

    if (isset($_GET["message"])) {
        $data['$message'] = $_GET["message"];
    }

    print render_template("tpl/list.html", $data);
} else if ($page == "addContact") {
    print render_template("tpl/add.html");
}else if ($page == "save"){
    $data = [];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $phones = array($_POST["phone1"]);
    if (!empty($_POST["phone2"])) {
        array_push($phones, $_POST["phone2"]);
    }

    if (!empty($_POST["phone3"])) {
        array_push($phones, $_POST["phone3"]);
    }
    $contactEntry = [$firstName, $lastName, $phones];
 //   if (!empty($contactEntry)){
        $errors = validate_contact($contactEntry);
  //      if (count($errors) == 0) {
            $contactsDAO->save($contactEntry);
            header("Location: ?command=contactList");
            //$contact = new Contact($contactEntry)
//        } else {
//            $data["$errors"] = $errors;
//        }
 //   }
} else {
    readfile("tpl/main.html");
}