<?php

$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$phones = array($_POST["phone1"]);
if (!empty($_POST["phone2"])) {
    array_push($phones, $_POST["phone2"]);
}

if (!empty($_POST["phone3"])) {
    array_push($phones, $_POST["phone3"]);
}

if (!empty($firstName) && (!empty($lastName)) && (!empty($phones))) {

    try {
//open the database
        $db = new PDO('sqlite:db1.sqlite');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Insert record
        $insert = "INSERT INTO contacts (firstName, lastName) VALUES (:firstName, :lastName);";
        $stmt = $db->prepare($insert);

        $stmt->bindParam('firstName', $firstName);
        $stmt->bindParam('lastName', $lastName);

        $stmt->execute();

        // get Autoincrement value
        //$id_value = $db->exec("select seq from sqlite_sequence where name=\"contacts\";");
        $id_value = $db->lastInsertId();

        foreach ($phones as $phone) {

            $insert = "INSERT INTO phones (contact_id, number) VALUES (:id_value, :phone);";
            $stmt = $db->prepare($insert);

            $stmt->bindParam('id_value', $id_value);
            $stmt->bindParam('phone', $phone);

            $stmt->execute();
            print_r($db->errorInfo());

        }
        // redirect to full list
        header("Location: index.php?command=show_list_page");
    } catch
    (PDOException $e) {
        print 'Exception : ' . $e->getMessage();
    }
}
$db = NULL;


