<?php
class Contact {
    public $id;
    public $firstName;
    public $lastName;
    public $phones = [];

    /**
     * Contact constructor.
     * @param $firstName
     * @param $lastName
     * @param $phones
     * @param null $id
     */
    public function __construct($firstName, $lastName, $phones, $id = null) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->id = $id;
        $this->phones = $phones;
    }
}