<?php
/**
 * Created by PhpStorm.
 * User: gertlohmus
 * Date: 2019-03-21
 * Time: 06:18
 */

class PersonEntry
{
    public $firstName;
    public $lastName;
    public $phone;
    public function __construct(
        $firstName, $lastName, $phone) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
    }

}