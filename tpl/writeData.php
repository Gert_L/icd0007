<?php

function add_data_entry($contacts) {
    if (isset($contacts)){
        file_put_contents(DATA_FILE, join(",", $contacts) . PHP_EOL, FILE_APPEND);
    }
}

$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$phone = $_POST["phone"];

if (!empty($firstName) && (!empty($lastName)) && (!empty($phone)))
    $contacts = array($firstName, $lastName, $phone);

if (isset($contacts)) {
    add_data_entry($contacts);
    header("Location: index.php?command=show_list_page");
    exit();
}
