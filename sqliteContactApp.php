<?php


class SqliteContactsDao implements contactsDAO
{

    const URL = "sqlite:db1.sqlite";

    private $connection;

    function __construct()
    {
        $this->connection = new PDO(self::URL);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function save($contactInfo)
    {
 //       try {
        $statement = $this->connection->prepare('insert into contacts (firstName, lastName) values (:firstName, :lastName)');
//        $statement->bindValue(':firstName', $contactInfo->firstName);
//        $statement->bindValue(':lastName', $contactInfo->lastName);
//        $statement->execute();

        $statement->bindParam('firstName', $contactInfo[0]);
        $statement->bindParam('lastName', $contactInfo[1]);

        $statement->execute();

        // get Autoincrement value
        //$id_value = $db->exec("select seq from sqlite_sequence where name=\"contacts\";");
        $id_value = $this->connection->lastInsertId();

        foreach ($contactInfo[2] as $phone) {

            $insert = "INSERT INTO phones (contact_id, number) VALUES (:id_value, :phone);";
            $statement = $this->connection->prepare($insert);

            $statement->bindParam('id_value', $id_value);
            $statement->bindParam('phone', $phone);

            $statement->execute();
    //        print_r($db->errorInfo());
        }
//    } catch (PDOException $e) {
//
//            }
    }

    function findAll()
    {
 //       try {
            $statement = $this->connection->prepare(
                'select c. id, c.firstname, c.lastname, group_concat(p.number) number
            from contacts c inner join phones p on p.contact_id = c.id
            group by c.id, c.firstname, c.lastname;');
            $statement->execute();

            $contacts = [];
            foreach ($statement as $row) {
                $numbers = explode(',', trim($row['number']));
                $item = new Contact($row['firstName'], $row['lastName'], $numbers, $row['id']);
                $contacts[] = $item;
                //$contacts[$item->id] = $item;
            }
            return $contacts;
//        } catch (PDOException $e) {
//            $contacts = [];
//            return $contacts;
//        }
    }

}